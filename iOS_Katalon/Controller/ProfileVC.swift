//
//  ProfileVC.swift
//  iOS_Katalon
//
//  Created by Kitiwat Chanluthin  on 15/1/2563 BE.
//  Copyright © 2563 WisdomVast. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    var imageName:String?
    var artistName:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.accessibilityLabel = "artist_ImageViewID"
        imageView.isAccessibilityElement = true
        
        titleLbl.accessibilityLabel = "artist_titleLblID"
        titleLbl.isAccessibilityElement = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.navigationController?.title = "Profile"
        if let imgName = self.imageName{
            if let image = UIImage(named: imgName){
                imageView.image = image
            }
        }
        
        titleLbl.text = artistName ?? "N/A"
    }
    
}

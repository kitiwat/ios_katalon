//
//  SimpleVC.swift
//  iOS_Katalon
//
//  Created by Kitiwat Chanluthin  on 14/1/2563 BE.
//  Copyright © 2563 WisdomVast. All rights reserved.
//

import UIKit


class Song{
    var id:Int?
    var name:String?
    var type:Deeplink?
}

enum Deeplink:String {
    case category
    case playlist
    //case banner
    case song
}

class SimpleVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let songHoCellId = "songHoCellId"
    
    var datas:[Song] = []
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
        for i in 0...100{
            let song = Song()
            song.id = i
            song.name = "song name \(i)"
            song.type = randType()
            datas.append(song)
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.accessibilityLabel = "kanom_tableView_ja"
        tableView.isAccessibilityElement = true
        
        let songHoNib = UINib(nibName: "SongHorizontalCell", bundle: nil)
        
        tableView.register(songHoNib, forCellReuseIdentifier: songHoCellId)
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
    }
    

    func randType()->Deeplink{
        let number = Int.random(in: 0 ..< 3)
        switch number {
        case 0:
            return .category
        case 1:
            return .playlist
//        case 2:
//            return .banner
        case 2:
            return .song
        default:
            return .song
        }
    }
    

}

extension SimpleVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let type = datas[indexPath.row].type
        
        switch type {
        case .playlist:
            let cell = tableView.dequeueReusableCell(withIdentifier: songHoCellId, for: indexPath) as! SongHorizontalCell
            cell.accessibilityLabel = "songHorizontal_cellId\(indexPath.row)"
            cell.isAccessibilityElement = true
            cell.delegate = self
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
            cell.accessibilityLabel = "cellId\(indexPath.row)_ja"
            cell.isAccessibilityElement = true
            let str = datas[indexPath.row].type?.rawValue
            cell.textLabel?.text = "no:\(indexPath.row) ->\(str ?? "nil")"
            return cell
        }
         
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let alert = UIAlertController(title: "Selected", message: "data is \(datas[indexPath.row])", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.destructive, handler: nil))
        
        
        let deeplink = datas[indexPath.row].type
        var vc:UIViewController?
        switch deeplink! {
        case .category:
            vc = CategoryVC(nibName: "CategoryVC", bundle: nil)
        case .playlist:
            vc = PlaylistVC(nibName: "PlaylistVC", bundle: nil)
        case .song:
            vc = PlaySongVC(nibName: "PlaySongVC", bundle: nil)
        default:
            break
        }
        
        self.navigationController?.pushViewController(vc!, animated: true)
       // self.present(alert, animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    

    
    
}

extension SimpleVC:SongHorizontalCellDelegate{
    func cellDidSelect(imageName: String) {
        let vc = ProfileVC(nibName: "ProfileVC", bundle: nil)
        vc.imageName = imageName
        vc.artistName = imageName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

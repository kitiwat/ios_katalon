//
//  PdfVC.swift
//  WiseRoom
//
//  Created by JellyNoon on 16/1/2563 BE.
//  Copyright © 2563 Wisdom Vast. All rights reserved.
//

import UIKit
import PDFKit
import AVFoundation
import AVKit

class PdfVC: UIViewController, PDFViewDelegate {
    
    @IBOutlet weak var progressBar: UISlider!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var progressTime: UILabel!
    @IBOutlet weak var controlBtn: UIButton!
    @IBOutlet weak var pdfContainer: UIView!
    
    var timeObserverToken: Any?
    var materialId: Int?
    var pdfView: PDFView!
    var pdfdocument:PDFDocument?
//    var presenter: PdfInterface!
    var audioPlayer: AVPlayer? = AVPlayer()
    var isPlaying = false
    var playItem: AVPlayerItem?
    private var shouldUpdatePDFScrollPosition = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        configure(PdfPresenter())
        guard let id = materialId else { return}
//        presenter.input.getMaterial(materialId: id)
        setupPdfView()
//        audioPlayer?.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)
        setupAudio()
    }
    
    deinit {
//        audioPlayer?.removeObserver(self, forKeyPath: "currentItem.loadedTimeRanges")
        removePeriodicTimeObserver()
//        if let currentPlayItem = playItem {
//            unregisterPlayItemObserver(currentPlayItem)
//        }
        audioPlayer?.pause()
        audioPlayer = nil
        print("deinit")
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavbar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        shouldUpdatePDFScrollPosition = false
    }
    
    func setupNavbar() {
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_close_btn"), style: .plain, target: self, action: #selector(closePressed))
    }
    
    @objc func closePressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: PDF View
    func downloadPdf() {
//        guard let path = presenter.output.material?.file?.url else {return}
//        let fileUrl = URL(string: path)
//        renderPdf(path: fileUrl!)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        pdfView.autoScales = true // This call is required to fix PDF document scale, seems to be bug inside PDFKit
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if shouldUpdatePDFScrollPosition {
            fixPDFViewScrollPosition()
        }
    }
    
    private func setupPdfView(){
        //        pdfView = PDFView(frame: CGRect(x: 0, y: 0, width: self.pdfContainer.frame.width, height: self.pdfContainer.frame.height))
        pdfView = PDFView()
        //        pdfView.alpha = 0.5
        pdfView.delegate = self
        self.pdfContainer.addSubview(pdfView)
        //        pdfView.frame = pdfContainer.frame
        
        pdfView.displayMode = PDFDisplayMode.singlePage
        pdfView.displayDirection = .vertical
        pdfView.autoScales = true
        
        let option = [ UIPageViewController.OptionsKey.interPageSpacing: 5]
        pdfView.usePageViewController(true, withViewOptions: option)
    }
    
    private func renderPdf(path: URL) {
        self.pdfdocument = PDFDocument(url: path)
        self.pdfView.document = self.pdfdocument
        self.pdfView.frame = pdfContainer.bounds
        if self.pdfView.document == nil{
            print("pdf not found")
            return
        }
    }
    
    func updatePdfFrame() {
        let topBarHeight = self.navigationController?.navigationBar.frame.height ?? 0.0
        let height = pdfContainer.bounds.size.height - topBarHeight
        pdfView.frame = CGRect(x: 0, y: 0, width: self.pdfContainer.frame.width, height: height)
    }
    
    private func fixPDFViewScrollPosition() {
        if let page = pdfView.document?.page(at: 0) {
            pdfView.go(to: PDFDestination(page: page, at: CGPoint(x: 0, y: page.bounds(for: pdfView.displayBox).size.height)))
        }
    }
    
    //MARK: AVAudioPlayer
    func setupAudio() {
        print("playAudio")
        let urlString = "https://wiseroom-stg.wisdomcloud.net/data/materialFile/KKwGALfDZHsMRwPlAfLfKiqV.mp3?m=MfIrf-6DM08nmruTubRN9w&e=1580116133"
        if let url = URL(string: urlString){
//            audioPlayer = AVPlayer(url: url)
            
            playItem = AVPlayerItem.init(url: url)
            audioPlayer?.play()
//            isPlaying = true
            regis(playItem!)
            
            addPeriodicTimeObserver()
        }
    }
    
    func regis(_ currentPlayItem: AVPlayerItem) {
        audioPlayer?.addObserver(self, forKeyPath: "currentItem.loadedTimeRanges", options: .new, context: nil)
//        currentPlayItem.addObserver(self, forKeyPath: "status", options: .new, context: nil)
//        audioPlayer?.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
        currentPlayItem.addObserver(self,
                                    forKeyPath: "status",
                                    options: .new,
                                    context: nil)
        
        currentPlayItem.addObserver(self,
                                    forKeyPath: "playbackBufferEmpty",
                                    options: .new,
                                    context: nil)
        
        currentPlayItem.addObserver(self,
                                    forKeyPath: "playbackLikelyToKeepUp",
                                    options: .new,
                                    context: nil)
        
        currentPlayItem.addObserver(self,
                                    forKeyPath: "playbackBufferFull",
                                    options: .new,
                                    context: nil)

    }
    
    func unregisterPlayItemObserver(_ currentPlayItem: AVPlayerItem) {
//        currentPlayItem.removeObserver(self, forKeyPath: "status")
        
    }

    
    func removePeriodicTimeObserver() {
        if let timeObserverToken = timeObserverToken {
            audioPlayer?.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
    }
    
    func addPeriodicTimeObserver() {
        let time = CMTime(value: 1, timescale: 1)
        timeObserverToken = audioPlayer?.addPeriodicTimeObserver(forInterval: time, queue: .main) {
            [weak self] time in
            let seconds = CMTimeGetSeconds(time)
            let secondsString = String(format: "%02d", Int(seconds) % 60)
            let minutesString = String(format: "%02d", Int(seconds) / 60)
            print("-- \(minutesString):\(secondsString)")
            self?.progressTime.text = "\(minutesString):\(secondsString)"
            if let duration = self?.audioPlayer?.currentItem?.duration{
                let durationSection = CMTimeGetSeconds(duration)
                self?.progressBar.value = Float(seconds / durationSection)
            }
        }
    }
    
    @IBAction func controlBtnPressed(_ sender: Any) {
        if isPlaying {
            audioPlayer?.pause()
            controlBtn.setImage(UIImage(named:"stop_btn"), for: .normal)
        } else {
            audioPlayer?.play()
            controlBtn.setImage(UIImage(named:"learn_btn"), for: .normal)
        }
        isPlaying = !isPlaying
    }
    
    @IBAction func progressChange(_ sender: Any) {
        print(" \(progressBar.value)")
        if let duration = audioPlayer?.currentItem?.duration {
            let totalSeconds = CMTimeGetSeconds(duration)
            
            let value = Float64(progressBar.value) * totalSeconds
            let seekTime = CMTime(value: CMTimeValue(value), timescale: 1)
            audioPlayer?.seek(to: seekTime, completionHandler: { (completeSeek) in
                
            })
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        print("keypath \(keyPath)")
    }
    
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        if keyPath == "currentItem.loadedTimeRanges" {
//            if let duration = audioPlayer?.currentItem?.duration{
//                let seconds = CMTimeGetSeconds(duration)
////                let secondsText = String(format: "%02d", Int(seconds) % 60)
////                let minutesText = String(format: "%02d", Int(seconds) / 60)
//                let (h, m, s) = Int(seconds).secondsToHoursMinutesSeconds()
////                print ("\(h) Hours, \(m) Minutes, \(s) Seconds")
//                let hour = String(format: "%02d", h)
//                let minute = String(format: "%02d", m)
//                let sec = String(format: "%02d", s)
//                if h <= 0 {
//                    self.duration.text = "\(minute):\(sec)"
//                } else {
//                    self.duration.text = "\(hour):\(minute):\(sec)"
//                }
//            }
//        }
//        if (keyPath == "status") {
//
//            if (playItem?.status.rawValue == AVPlayer.Status.readyToPlay.rawValue) {
////                let totalDuration = Int((round((videoPlayer.currentItem?.duration.seconds)!)))
////                mPresenter.onPlayerIsReady(videoDuration: totalDuration)
//
//                // MARK: Legacy code (MVC)
////                isSeeking = false
////                if playWhenReady {
////                    videoPlayer.play()
////                    sendNotifClipPlayig()
////                }
//
//            } else if (playItem?.status.rawValue == AVPlayer.Status.failed.rawValue) {
//                print("failed \(AVPlayer.Status.failed.rawValue)")
////                mPresenter.onPlayerStopped()
////                mPresenter.onPlayerFailed(debugSongId)
//            } else if (playItem?.status.rawValue == AVPlayer.Status.unknown.rawValue) {
//                print("unknow \(AVPlayer.Status.unknown.rawValue)")
////                mPresenter.onPlayerStopped()
////                mPresenter.onPlayerFailed(debugSongId)
//            }
//        } else if (keyPath == "playbackBufferEmpty") {
//            if (playItem?.isPlaybackBufferEmpty)! {
////                mPresenter.onPlayerPlaybackBufferEmpty()
//            }
//
//        } else if (keyPath == "playbackLikelyToKeepUp") {
////            if !isSeeking {
////                mPresenter.onPlayerLikelyToKeepUp()
////            }
//
//        } else if (keyPath == "playbackBufferFull") {
//
//        } else if (keyPath == "rate") {
//            if let rate = change?[NSKeyValueChangeKey.newKey] as? Float {
//                if rate == 0.0 {
////                    log.debug("playback stopped")
////                    mPresenter.onPlayerStopped()
//                }
//                if rate == 1.0 {
////                    log.debug("normal playback")
////                    mPresenter.onPlayerPlaybackAtNormalSpeed()
//                }
//                if rate == -1.0 {
////                    log.debug("reverse playback")
//                }
//            }
//
////            mediaController?.updatePlayPause()
//        }
//    }
    
    //MARK: Presenter
//    func configure(_ interface: PdfInterface) {
//        self.presenter = interface
//        bindPresenter()
//    }
    
    func bindPresenter() {
//        presenter.output.getPdfMaterialSuccess = getPdfMaterialSuccess()
//        presenter.output.getPdfMaterialError = getPdfMaterialError()
//        presenter.output.startLoding = startLoading()
//        presenter.output.stopLoading = stopLoading()
//    }
    
    func getPdfMaterialSuccess() -> ((String) -> ()) {
        return { [weak self] message in
            self?.downloadPdf()
            self?.updatePdfFrame()
//            self?.playAudio()
        }
    }
    
    func getPdfMaterialError() -> ((String) -> ()) {
        return { [weak self] message in
        }
    }
    
    func stopLoading() -> (() -> Void) {
        return { [weak self] in
        }
    }
    
    func startLoading() -> (() -> Void) {
        return { [weak self] in
        }
    }
}

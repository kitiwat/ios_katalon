//
//  VideoVC.swift
//  WiseRoom
//
//  Created by JellyNoon on 16/1/2563 BE.
//  Copyright © 2563 Wisdom Vast. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class VideoVC: AVPlayerViewController, AVPlayerViewControllerDelegate {

    var materialId: Int?
   // var presenter: VideoInterface!
    var videoDelegate: VideoPlayerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // configure(VideoPresenter())
        
        guard let id = materialId else { return}
      //  presenter.input.getVideo(materialId: id)
        
        player?.addObserver(self, forKeyPath: "status", options: .new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "status") {
            let status: AVPlayer.Status = self.player!.status
            if(status == AVPlayer.Status.readyToPlay){
                player?.play()
                print("Ready to play")
            }
            else{
                if( status == AVPlayer.Status.unknown){
                    print("failed")
                }
            }
        }
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        guard let oreintation = presenter.output.video?.file?.orientation else {return UIInterfaceOrientationMask()}
//        if oreintation == "portrait" {
//            return .portrait
//        } else {
//            return .landscapeRight
//        }
    }

    override var shouldAutorotate: Bool {
        return true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        videoDelegate?.dismissVideoPlayer()

//        let value = UIInterfaceOrientation.portrait.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    func playVideo() {
        guard let path = presenter.output.video?.file?.url
            ,let vdoUrl = URL(string: path)
            else { return}
        let player = AVPlayer(url: vdoUrl)
        
         self.player = player
    }
    
    func configure(_ interface: VideoInterface) {
        self.presenter = interface
        bindPresenter()
    }
    
    func bindPresenter() {
        presenter.output.getVideoSuccess = getVideoSuccess()
        presenter.output.getVideoError = getVideoError()
    }
    
    func getVideoSuccess() -> ((String) -> ()) {
        return { [weak self] message in
            self?.playVideo()
        }
    }
    
    func getVideoError() -> ((String) -> ()) {
        return { [weak self] message in
        }
    }
}

extension UINavigationController {
    
    override open var shouldAutorotate: Bool {
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.shouldAutorotate
            }
            return super.shouldAutorotate
        }
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.preferredInterfaceOrientationForPresentation
            }
            return super.preferredInterfaceOrientationForPresentation
        }
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.supportedInterfaceOrientations
            }
            return super.supportedInterfaceOrientations
        }
    }
}

protocol VideoPlayerDelegate: NSObjectProtocol {
    func dismissVideoPlayer()
}

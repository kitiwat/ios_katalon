//
//  HomeVC.swift
//  iOS_Katalon
//
//  Created by Kitiwat Chanluthin  on 27/1/2563 BE.
//  Copyright © 2563 WisdomVast. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let songHor = "songHor"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
    }

    func setupTableView(){
        
        let songNib = UINib(nibName: "SongHorizontalScrollView", bundle: nil)
        tableView.register(songNib, forCellReuseIdentifier: songHor)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.addRestorationId(withId: "tableView_id000")
    }
   
}

extension HomeVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: songHor, for: indexPath) as! SongHorizontalScrollView
        cell.contentView.backgroundColor = .brown
        cell.addRestorationId(withId: "cellId_\(indexPath.row)")
        return cell
    }
    
    
}

//
//  SongCoverAndNameView.swift
//  AIS Karaoke
//
//  Created by Kitiwat Chanluthin  on 10/5/2562 BE.
//  Copyright © 2562 WisdomVast. All rights reserved.
//

import UIKit



class ShadowView: UIView {
    
}

class PaddingLabel:UILabel{
    
}

class SongCoverAndNameView: UIView {
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var containerView: ShadowView!
    //    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subtitleLbl: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var microphoneImageView: UIImageView!
    
    @IBOutlet weak var tagLbl: PaddingLabel!
    
    @IBOutlet weak var numberOfListenLbl: UILabel!
//    @IBOutlet weak var containerView: UIView!
    
    var tagType:Int? = nil{
        didSet{
            if tagType == nil{
                tagLbl.isHidden = true
            }else{
                tagLbl.isHidden = false
            }
        }
    }
 
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("SongCoverAndNameView", owner: self, options: nil)
        addSubview(contentView)
        
//        self.accessibilityLabel = "\(self)"
//        self.isAccessibilityElement = true
//        
//        self.imageView.accessibilityLabel = "imageView"
//        self.imageView.isAccessibilityElement = true
//        
//        self.titleLbl.accessibilityLabel = "titleLbl"
//        self.titleLbl.isAccessibilityElement = true
//        
//        
//        self.subtitleLbl.accessibilityLabel = "subtitleLbl"
//        self.subtitleLbl.isAccessibilityElement = true
//        
//        self.moreBtn.accessibilityLabel = "moreBtn"
//        self.moreBtn.isAccessibilityElement = true
//        
//        self.microphoneImageView.accessibilityLabel = "microphoneImageView"
//        self.microphoneImageView.isAccessibilityElement = true
//        
//        self.tagLbl.accessibilityLabel = "tagLbl"
//        self.tagLbl.isAccessibilityElement = true
//        
//        self.numberOfListenLbl.accessibilityLabel = "numberOfListenLbl"
//        self.numberOfListenLbl.isAccessibilityElement = true
        
        
        tagType = nil
        if let placeHolderImage = UIImage(named: "placeholder"){
            imageView.image = placeHolderImage
            imageView.contentMode = .center
            
        }
        contentView.frame = self.bounds
        contentView.backgroundColor = .clear
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    
        if let image = UIImage(named: "ic_more_vert_black")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate){
           
            moreBtn.setImage(image, for: [])
            moreBtn.tintColor = .lightGray
        }
        moreBtn.isEnabled = false
//        numberOfListenLbl.textDropShadow()
        containerView.backgroundColor = .clear
    }
    
   
    @IBAction func moreBtnTap(_ sender: Any) {
        print("tap")
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let cornerRadius : CGFloat = 6.0
        let color = UIColor.darkGray
        containerView.clipsToBounds = false
        
//        if !Utils.shared.deniedShadow(){
//            containerView.layer.shadowColor = color.cgColor
//            containerView.layer.shadowOffset = CGSize(width: 5, height: 5)
//            containerView.layer.shadowRadius = 1
//            containerView.layer.shadowOpacity = 0.2
//            containerView.layer.shadowPath = UIBezierPath(roundedRect: containerView.bounds, cornerRadius: cornerRadius).cgPath
//        }
        
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = cornerRadius
    }
    
}



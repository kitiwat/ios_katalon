//
//  CollectionCell.swift
//  AIS Karaoke
//
//  Created by Kitiwat Chanluthin  on 11/5/2562 BE.
//  Copyright © 2562 WisdomVast. All rights reserved.
//

import UIKit
import Kingfisher

class SongHorizontalScrollView: UITableViewCell {
   
    @IBOutlet weak var collectionView: UICollectionView!
    
    var flag:String = ""
    let cellId = "SongCoverAndNameView"
    
     var items:[AnyObject] = []
    
//    var items:[AnyObject] = []{
//        didSet{
//            if items.isEmpty{
//                state = .noData
//            }else{
//                state = .loaded
//            }
//        }
//    }
    
//    var state: State = .noData {
//        didSet {
//            switch state {
//            case .noData:
//                collectionView.setEmptyStateView(title: "", message: "",flag:self.flag, action: {
//
//                })
//            case .loaded:
//                collectionView.restore()
//                collectionView.reloadData()
//            case .error(let error):
//                self.collectionView.setErrorStateView(title: "", message: error, action: {
//                  //  self.getListCategory()
//                })
//            case .loading:
//                collectionView.restore()
//            }
//        }
//    }
//
//    let isIphone = UIDevice.current.iPhone

    func configure(data: [AnyObject],flag:String) {
        self.flag = flag
        self.items = data
        collectionView.reloadData()
    }
    
    func debug(){
        let obj0 = NSObject()
        
        self.items = [obj0,obj0,obj0,obj0,obj0,obj0,obj0,obj0,obj0]
        collectionView.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
         self.backgroundColor = .red
       
        setupCollectionView()
        debug()
    }
    
    private func setupCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
        collectionView.contentInset = UIEdgeInsets(top: 6, left: 16, bottom: 6, right: 0)
        collectionView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isScrollEnabled = true
 
        collectionView.register(CellContainer.self, forCellWithReuseIdentifier: cellId)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
       // collectionView.backgroundColor = .yellow
        
//        if flag == FLAG_PROFILE{
//            collectionView.backgroundColor = .white
//        }else{
//            collectionView.backgroundColor = .white
        //}
        
    }

    
}

extension SongHorizontalScrollView:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let view = collectionView.cellForItem(at: indexPath) else {return}
//        if let song = items[indexPath.item] as? SongCommonAttribute{
//
//            PlayerHelper.attemptPlaySong(song: song, anchorView:view, flag: "SongHorizontalScrollView")
//
//            if flag == FLAG_PROFILE {
//                FirebaseControl.instance.send(eventName: FirebaseControl.EVENT_CLICK_RECENT_SONG, key: FirebaseControl.KEY_SONG_ID, val: "\(song.id)")
//            } else
//            {
//                FirebaseControl.instance.send(eventName: FirebaseControl.EVENT_CLICK_SONG, key: FirebaseControl.KEY_SONG_ID, val: "\(song.id)")
//            }
//
//        }else if let album = items[indexPath.item] as? Album{
//            var data = [String:Album]()
//             data["key0"] = album
//            NotificationCenter.default.post(name: .albumDidTap, object: nil,userInfo:["key0": album])
//        }else{
//            print("don't know collection view type")
//        }
       // print("song id is \(song.id)")
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CellContainer
        cell.configure(item: items[indexPath.item],flag:"flag\(indexPath.item)")
//        cell.accessibilityLabel = "\(self)_collectionView_cell_Item\(indexPath.item)"
//        cell.isAccessibilityElement = true
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        let numOfCol:CGFloat = isIphone ? 2.9 : 6.1
        let numOfCol:CGFloat = 2.9
        let height = collectionView.frame.height * 1
        let width = collectionView.frame.width / numOfCol // almost 3 item
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    
}

class CellContainer:UICollectionViewCell{
    
    var songView:SongCoverAndNameView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var songId: String!
    func addViews(){
        songView = SongCoverAndNameView()
        self.contentView.addSubview(songView)
        songView.frame = self.bounds
        songView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        songView.backgroundColor = .lightGray
//        songView.titleLbl.text = "title"
//                songView.subtitleLbl.text = "subtitle"
//        songView.moreBtn.addTargetClosure { (button) in
//            guard let songId = self.songId else { return }
//
//            FirebaseControl.instance.send(eventName: FirebaseControl.EVENT_CLICK_SONG_OPTIONS, key: FirebaseControl.KEY_SONG_ID, val: songId)
//        }
    }
    
    func configure(item:AnyObject,flag:String){
        
        var path:String = ""
        var title:String = ""
        var statusTag:Int = -1
        var subtitle:String = ""
        
        var numOfListener = ""
        var numOfSongs = ""
        
//        if let song =  item as? SongCommonAttribute{
//            guard let songPath = song.coverUrl,
//                let tag = song.statusTag,
//                let songTitle = song.title,
//                let listener = song.numOfListener,
//                let songArtist = song.artist,
//                let songId = song.id
//                else {
//                    return
//            }
//            
//            
//            statusTag = tag
//            path = songPath
//            title = songTitle
//            subtitle = songArtist
//            numOfListener = listener
//            
//            self.songId = "\(songId)"
//            
//        }else if let album = item as? Album{
//            guard let coverUrl = album.coverUrl,
//                let albumTitle = album.title,
//                let albumNumOfSongs = album.numOfSong
//                else{
//                    return
//            }
//            // subtitle = "\(albumNumOfSongs)"
//            subtitle = ""
//            path = coverUrl
//            title = albumTitle
//        }
//        
        songView.titleLbl.text = flag
        songView.subtitleLbl.text = "subtitle" + flag
        songView.imageView.image = UIImage(named: "toki")
        songView.imageView.contentMode = .scaleAspectFill
//        songView.numberOfListenLbl.text = numOfListener
//        songView.imageView.setImage(withUrl: path, size: songView.imageView.frame.size)

    }
    
    
}

//
//  DiploCell.swift
//  iOS_Katalon
//
//  Created by Kitiwat Chanluthin  on 14/1/2563 BE.
//  Copyright © 2563 WisdomVast. All rights reserved.
//

import UIKit


class DiploCell: UICollectionViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var desLbl: UILabel!
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImage.addRestorationId(withId: "profileImage_in_diplo")
    }
    
    func setrestorationId(resId:String){
       // desLbl.text = resId
       // titleLbl.text = resId
      //  subTitleLbl.text = resId
    }

}

//
//  SongHorizontalCell.swift
//  iOS_Katalon
//
//  Created by Kitiwat Chanluthin  on 14/1/2563 BE.
//  Copyright © 2563 WisdomVast. All rights reserved.
//

import UIKit

protocol SongHorizontalCellDelegate:NSObjectProtocol {
    func cellDidSelect(imageName:String)
}

class SongHorizontalCell: UITableViewCell {

    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let margin: CGFloat = 10
    let cellsPerRow = 5
    
    let cellId = "cellId"
    var images = ["diplo","billie","dave","taylor","toki"]
    
    weak var delegate:SongHorizontalCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        images.shuffle()
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal //.horizontal
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        collectionView.setCollectionViewLayout(layout, animated: true)

        
        collectionView.delegate = self
        collectionView.dataSource = self
        let nib = UINib(nibName: "DiploCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: cellId)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension SongHorizontalCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageName = images[indexPath.row]
        delegate?.cellDidSelect(imageName:imageName)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! DiploCell
        let resId = "SngHor_ClId\(indexPath.row)"
        let imageName = images[indexPath.row]
        cell.profileImage.image = UIImage(named: imageName)
        cell.titleLbl.text =  imageName
       
        cell.accessibilityLabel = resId
        cell.isAccessibilityElement = true
        cell.setrestorationId(resId: resId)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 1.0, left: 1.0, bottom: 1.0, right: 1.0)//here your custom value for spacing
        }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                let lay = collectionViewLayout as! UICollectionViewFlowLayout
                let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing

    return CGSize(width:widthPerItem, height:200)
    }
    
}
